# Install script for directory: /home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp-build/libcryptopp.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/cryptopp" TYPE FILE FILES
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/3way.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/adler32.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/aes.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/algebra.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/algparam.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/arc4.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/argnames.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/asn.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/authenc.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/base32.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/base64.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/basecode.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/bench.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/blake2.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/blowfish.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/blumshub.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/camellia.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/cast.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/cbcmac.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/ccm.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/chacha.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/channels.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/cmac.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/config.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/cpu.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/crc.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/cryptlib.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/default.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/des.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/dh.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/dh2.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/dll.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/dmac.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/drbg.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/dsa.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/eax.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/ec2n.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/eccrypto.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/ecp.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/ecpoint.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/elgamal.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/emsa2.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/eprecomp.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/esign.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/factory.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/fhmqv.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/files.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/filters.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/fips140.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/fltrimpl.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/gcm.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/gf256.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/gf2_32.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/gf2n.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/gfpcrypt.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/gost.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/gzip.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/hex.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/hkdf.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/hmac.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/hmqv.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/hrtimer.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/ida.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/idea.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/integer.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/iterhash.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/keccak.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/lubyrack.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/luc.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/mars.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/md2.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/md4.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/md5.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/mdc.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/mersenne.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/misc.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/modarith.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/modes.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/modexppc.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/mqueue.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/mqv.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/nbtheory.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/network.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/nr.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/oaep.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/oids.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/osrng.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/ossig.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/panama.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/pch.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/pkcspad.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/poly1305.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/polynomi.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/pssr.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/pubkey.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/pwdbased.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/queue.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/rabin.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/randpool.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/rc2.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/rc5.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/rc6.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/rdrand.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/resource.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/rijndael.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/ripemd.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/rng.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/rsa.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/rw.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/safer.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/salsa.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/seal.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/secblock.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/seckey.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/seed.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/serpent.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/serpentp.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/sha.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/sha3.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/shacal2.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/shark.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/simple.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/siphash.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/skipjack.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/smartptr.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/socketft.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/sosemanuk.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/square.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/stdcpp.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/strciphr.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/tea.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/tiger.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/trap.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/trdlocal.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/trunhash.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/ttmac.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/twofish.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/validate.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/vmac.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/wait.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/wake.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/whrlpool.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/winpipes.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/words.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/xtr.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/xtrcrypt.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/zdeflate.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/zinflate.h"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/zlib.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/cryptopp" TYPE FILE FILES
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp/cryptopp-config.cmake"
    "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp-build/cryptopp-config-version.cmake"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/cryptopp/cryptopp-targets.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/cryptopp/cryptopp-targets.cmake"
         "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp-build/CMakeFiles/Export/lib/cmake/cryptopp/cryptopp-targets.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/cryptopp/cryptopp-targets-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/cryptopp/cryptopp-targets.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/cryptopp" TYPE FILE FILES "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp-build/CMakeFiles/Export/lib/cmake/cryptopp/cryptopp-targets.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/cryptopp" TYPE FILE FILES "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp-build/CMakeFiles/Export/lib/cmake/cryptopp/cryptopp-targets-release.cmake")
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/cryptopp-build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
