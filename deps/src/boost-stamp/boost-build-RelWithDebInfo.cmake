

set(command "./b2;stage;threading=multi;link=static;variant=release;address-model=64;--with-chrono;--with-date_time;--with-filesystem;--with-random;--with-regex;--with-test;--with-thread")
execute_process(
  COMMAND ${command}
  RESULT_VARIABLE result
  OUTPUT_FILE "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/boost-stamp/boost-build-out.log"
  ERROR_FILE "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/boost-stamp/boost-build-err.log"
  )
if(result)
  set(msg "Command failed: ${result}\n")
  foreach(arg IN LISTS command)
    set(msg "${msg} '${arg}'")
  endforeach()
  set(msg "${msg}\nSee also\n  /home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/boost-stamp/boost-build-*.log")
  message(FATAL_ERROR "${msg}")
else()
  set(msg "boost build command succeeded.  See also /home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/boost-stamp/boost-build-*.log")
  message(STATUS "${msg}")
endif()
