

set(command "/usr/bin/cmake;-DCMAKE_INSTALL_PREFIX=/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps;-DCMAKE_POSITION_INDEPENDENT_CODE=;-GUnix Makefiles;/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/secp256k1")
execute_process(
  COMMAND ${command}
  RESULT_VARIABLE result
  OUTPUT_FILE "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/secp256k1-stamp/secp256k1-configure-out.log"
  ERROR_FILE "/home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/secp256k1-stamp/secp256k1-configure-err.log"
  )
if(result)
  set(msg "Command failed: ${result}\n")
  foreach(arg IN LISTS command)
    set(msg "${msg} '${arg}'")
  endforeach()
  set(msg "${msg}\nSee also\n  /home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/secp256k1-stamp/secp256k1-configure-*.log")
  message(FATAL_ERROR "${msg}")
else()
  set(msg "secp256k1 configure command succeeded.  See also /home/gpumining/ssd/chinwee/Ethminer_dma_cpu3/ethminer/deps/src/secp256k1-stamp/secp256k1-configure-*.log")
  message(STATUS "${msg}")
endif()
