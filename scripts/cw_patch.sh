
sed -i 's/-Werror//g' $(pwd)/eth/CMakeFiles/eth.dir/flags.make
sed -i 's/-Werror//g' $(pwd)/libethash/CMakeFiles/ethash.dir/flags.make
sed -i 's/-Werror//g' $(pwd)/libethashseal/CMakeFiles/ethashseal.dir/flags.make
sed -i 's/-Wno-unknown-pragmas/-Wno-unknown-pragmas -fpermissive/g' $(pwd)/eth/CMakeFiles/eth.dir/flags.make
sed -i 's/-Werror//g' $(pwd)/libevm/CMakeFiles/evm.dir/flags.make
sed -i 's/-Werror//g' $(pwd)/test/CMakeFiles/testeth.dir/flags.make
sed -i 's/-Werror//g' $(pwd)/ethminer/CMakeFiles/ethminer.dir/flags.make
sed -i 's/-Wno-unknown-pragmas/-Wno-unknown-pragmas -fpermissive/g' $(pwd)/ethminer/CMakeFiles/ethminer.dir/flags.make

#enable SINGLE HASH
#sed -i 's/ -DSW_Mining_Only//g' $(pwd)/ethminer/CMakeFiles/ethminer.dir/flags.make
#sed -i 's/ -DFPGA_Mining_Only//g' $(pwd)/ethminer/CMakeFiles/ethminer.dir/flags.make
#sed -i 's/-DETH_RELEASE/-DETH_RELEASE -DSINGLE_HASH/g' $(pwd)/ethminer/CMakeFiles/ethminer.dir/flags.make
#sed -i 's/-DETH_RELEASE/-DETH_RELEASE -DSINGLE_HASH/g' $(pwd)/libethashseal/CMakeFiles/ethashseal.dir/flags.make

#enable FPGA only
#sed -i 's/ -DSINGLE_HASH//g' $(pwd)/ethminer/CMakeFiles/ethminer.dir/flags.make
#sed -i 's/ -DSW_Mining_Only//g' $(pwd)/ethminer/CMakeFiles/ethminer.dir/flags.make
#sed -i 's/-DETH_RELEASE/-DETH_RELEASE -DFPGA_Mining_Only/g' $(pwd)/ethminer/CMakeFiles/ethminer.dir/flags.make

#enable SW Mining only
sed -i 's/ -DSINGLE_HASH//g' $(pwd)/ethminer/CMakeFiles/ethminer.dir/flags.make
sed -i 's/ -DFPGA_Mining_Only//g' $(pwd)/ethminer/CMakeFiles/ethminer.dir/flags.make
sed -i 's/-DETH_RELEASE/-DETH_RELEASE -DSW_Mining_Only/g' $(pwd)/ethminer/CMakeFiles/ethminer.dir/flags.make
