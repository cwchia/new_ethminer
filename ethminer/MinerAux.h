#pragma once

/*
	This file is part of cpp-ethereum.

	cpp-ethereum is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	cpp-ethereum is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with cpp-ethereum.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file MinerAux.cpp
 * @author Gav Wood <i@gavwood.com>
 * @date 2014
 * CLI module for mining.
 */

#include <boost/algorithm/string/case_conv.hpp>
#include <libdevcore/CommonJS.h>
#include <libethcore/BasicAuthority.h>
#include <libethcore/Exceptions.h>
#include <libethashseal/EthashCPUMiner.h>
#include <jsonrpccpp/client/connectors/httpclient.h>
#include "FarmClient.h"

/*
 * This file contains APIs to interactive with Xilinx PCIe DMA/Bridge
 * Subsystem device driver
 *
 * Copyright (c) 2019-present,  Avnet
 * All rights reserved.
 *
 * This source code is licensed under both the BSD-style license (found in the
 * LICENSE file in the root directory of this source tree) and the GPLv2 (found
 * in the COPYING file in the root directory of this source tree).
 * You may select, at your option, one of the above-listed licenses.
 */

#define _BSD_SOURCE
#define _XOPEN_SOURCE 500
#include <assert.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#define _BSD_SOURCE
//#define _XOPEN_SOURCE 500
#include <assert.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include "xdma_api.h"



#define RESET_ACCEL
int verbose = 0;


#define SIZE_DEFAULT 						(32)
#define CHAN_DEFAULT 						(CHAN_H2C_0)
#define ACCEL_EVENT 						(USR_EVENT_0)
#define ACCEL_DEFAULT						(ACCEL_0)
#define AXILITE_MASTER_MAP_SIZE	( 5 * 64 * 1024UL)



#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", __LINE__, __FILE__, errno, strerror(errno)); exit(1); } while(0)

const char h2c_devname[CHAN_H2C_MAX][DEVNODE_NAME_MAX] = {
	"/dev/xdma0_h2c_0",
	"/dev/xdma0_h2c_1",
	"/dev/xdma0_h2c_2",
	"/dev/xdma0_h2c_3",
};

const char c2h_devname[CHAN_C2H_MAX][DEVNODE_NAME_MAX] = {
	"/dev/xdma0_c2h_0",
	"/dev/xdma0_c2h_1",
	"/dev/xdma0_c2h_2",
	"/dev/xdma0_c2h_3",
};

const char usr_event_devname[USR_EVENT_MAX][DEVNODE_NAME_MAX] = {
	"/dev/xdma0_events_0",
	"/dev/xdma0_events_1",
	"/dev/xdma0_events_2",
	"/dev/xdma0_events_3",
};

const char axilite_master_devname[AXILITE_MASTER_MAX][DEVNODE_NAME_MAX] = {
	"/dev/xdma0_user",
};

#define AXI_GPIO_REG_BASE	(0x00010000UL)
#define GPIO_DATA_REG		(AXI_GPIO_REG_BASE)
#define AXI_BRAM_BASE		(0x00000000UL)
#define BRAM_START_ADDR	(AXI_BRAM_BASE)
#define ACCEL_RDY_OFS		(0)		// Accelerator ready status offset
#define ACCEL_SIG_OFS		(4)		// Accelerator ready status offset
#define AXI_REG_RD_WR_BASE_0		(0x00000000UL)
#define REG_RD_WR_ADDR_0	(AXI_REG_RD_WR_BASE_0)
#define AXI_REG_RD_WR_BASE_1		(0x00010000UL)
#define REG_RD_WR_ADDR_1	(AXI_REG_RD_WR_BASE_1)
#define AXI_REG_RD_WR_BASE_2		(0x00020000UL)
#define REG_RD_WR_ADDR_2	(AXI_REG_RD_WR_BASE_2)
#define AXI_REG_RD_WR_BASE_3		(0x00030000UL)
#define REG_RD_WR_ADDR_3	(AXI_REG_RD_WR_BASE_3)
#define AXI_REG_RD_WR_BASE_4		(0x00040000UL)
#define REG_RD_WR_ADDR_4	(AXI_REG_RD_WR_BASE_4)
/**
 * xdma_channel_open - Open PCIe DMA channel
 * @channel: DMA channel number (0 -3)
 * @dir: DMA direction (0 - host to card, 1 - card to host)
 *
 * Return: On success, retrun non-negative integer representing file
 * descriptor of the device. Otherwise, return -ve value
 */
int xdma_channel_open(int channel, int dir)
{
	const char *devname;

	if (dir == DMA_DIR_H2C) {
		devname = h2c_devname[channel];
	}
	else {
		devname = c2h_devname[channel];
	}
	return open(devname, O_RDWR);
}

/**
 * xdma_channel_close - Close PCIe DMA channel
 * @fd: File descriptor of the device
 *
 */
void xdma_channel_close(int fd)
{
	close(fd);
}

/**
 * write_from_buffer - Write data to device node
 * fname: Any string for debug purpose
 * @fd: File descriptor of the device
 * @buffer: Source address (must align to 4KB boundary)
 * @size: Transfer size in byte
 * @base: Destination address on device node
 *
 * Return: On success, return transfered size. Otherwise, return
 * -ve value
 */
ssize_t write_from_buffer(char *fname, int fd, char *buffer,
		uint64_t size, uint64_t base)
{
	ssize_t rc;
	uint64_t count = 0;
	char *buf = buffer;
	off_t offset = base;

	while (count < size) {
		uint64_t bytes = size - count;

		if (bytes > RW_MAX_SIZE)
			bytes = RW_MAX_SIZE;

		if (offset) {
			rc = lseek(fd, offset, SEEK_SET);
			if (rc != offset) {
				fprintf(stderr, "%s, seek off 0x%lx != 0x%lx.\n",
					fname, rc, offset);
				perror("seek file");
				return -EIO;
			}
		}

		/* write data to file from memory buffer */
		rc = write(fd, buf, bytes);
		if (rc != bytes) {
			fprintf(stderr, "%s, W off 0x%lx, 0x%lx != 0x%lx.\n",
				fname, offset, rc, bytes);
				perror("write file");
			return -EIO;
		}
		count += bytes;
		buf += bytes;
		offset += bytes;
	}

	if (count != size) {
		fprintf(stderr, "%s, R failed 0x%lx != 0x%lx.\n",
				fname, count, size);
		return -EIO;
	}
	return count;
}

/**
 * xdma_h2c_transfer - DMA transfer from host to card
 * @fd: File descriptor of the device
 * @buffer: Source address (must align to 4KB boundary)
 * @size: Transfer size in byte
 * @dest_addr: Destination address on dma device
 *
 * Return: On success, return transfered size. Otherwise, return
 * -ve value
 */
ssize_t xdma_h2c_transfer(int fd, char *buffer, uint64_t size, uint64_t dest_addr)
{
	return write_from_buffer("h2c chan", fd, buffer, size, dest_addr);
}

/**
 * read_to_buffer - Read data from device node
 * @fname: Any string for debug purpose
 * @fd: File descriptor of the device
 * @buffer: Destination address (must align to 4KB boundary)
 * @size: Transfer size in byte
 * @base: Source address from device
 *
 * Return: On success, return transfered size. Otherwise, return
 * -ve value
 */
ssize_t read_to_buffer(char *fname, int fd, char *buffer, uint64_t size,
			uint64_t base)
{
	ssize_t rc;
	uint64_t count = 0;
	char *buf = buffer;
	off_t offset = base;

	while (count < size) {
		uint64_t bytes = size - count;

		if (bytes > RW_MAX_SIZE)
			bytes = RW_MAX_SIZE;

		if (offset) {
			rc = lseek(fd, offset, SEEK_SET);
			if (rc != offset) {
				fprintf(stderr, "%s, seek off 0x%lx != 0x%lx.\n",
					fname, rc, offset);
				perror("seek file");
				return -EIO;
			}
		}

		/* read data from file into memory buffer */
		rc = read(fd, buf, bytes);
		if (rc != bytes) {
			fprintf(stderr, "%s, R off 0x%lx, 0x%lx != 0x%lx.\n",
				fname, count, rc, bytes);
				perror("read file");
			return -EIO;
		}

		count += bytes;
		buf += bytes;
		offset += bytes;
	}

	if (count != size) {
		fprintf(stderr, "%s, R failed 0x%lx != 0x%lx.\n",
				fname, count, size);
		return -EIO;
	}
	return count;
}

/**
 * xdma_c2h_transfer - DMA transfer from card to host
 * @fd: File descriptor of the device
 * @buffer: Destination address (must align to 4KB boundary)
 * @size: Transfer size in byte
 * @src_addr: Source address from dma device
 *
 * Return: On success, return transfered size. Otherwise, return
 * -ve value
 */
ssize_t xdma_c2h_transfer(int fd, char *buffer, uint64_t size, uint64_t src_addr)
{
	return read_to_buffer("c2h_chan", fd, buffer, size, src_addr);
}

/**
 * xdma_axilite_master_open - Open axilite master device
 * @id: User control device id (should be 0)
 * Return: On success, retrun non-negative integer representing file
 * descriptor of control port. Otherwise, return -ve value
 */
int xdma_axilite_master_open(int id)
{
	const char *devname;
	devname = axilite_master_devname[id];
	return open(devname, O_RDWR | O_SYNC);
}

/**
 * xdma_axilite_master_close - Close axilite master device
 * @fd: File descriptor of user control device
 *
 */
void xdma_axilite_master_close(int fd)
{
	close(fd);
}

/**
 * xdma_axilite_master_mmap - Mapping bar address to virtual space
 * @fd: File descriptor of control port
 * @size: Map region size
 * @map_base: Mapped address base
 * @offset: Offset to file start
 *
 * Return: On success, return 0. Otherwise, deadloop
 */
int xdma_axilite_master_mmap(int fd, size_t size, void **map_base, off_t offset)
{
	void *map_base_t;

	map_base_t = mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, offset);
	if (map_base_t == (void *)-1)
		FATAL;
	//printf("Memory mapped at address %p.\n", map_base_t);

	*map_base = map_base_t;
	return 0;
}

/**
 * xdma_axilite_master_unmmap - Mapping bar address to virtual space
 * @map_base: Mapped address base
 * @size: Map region size
 *
 * Return: On success, return 0. Otherwise, deadloop
 */
int xdma_axilite_master_mumap(void *map_base, size_t size)
{
	if (munmap(map_base, size) == -1)
		FATAL;
	return 0;
}

/**
 * xdma_user_event_open - Open user event device
 * @event_num: User event num (0 - 7)
  *
 * Return: On success, retrun non-negative integer representing file
 * descriptor of the device. Otherwise, return -ve value
 */
int xdma_usr_event_open(int event_num)
{
	const char *devname;
	devname = usr_event_devname[event_num];
	//printf("event devnode %s\n", devname); // testing
	return open(devname, O_RDWR | O_SYNC);
}

/**
 * xdma_usr_event_close - Close user event device
 * @fd: File descriptor of the device
 *
 */
void xdma_usr_event_close(int fd)
{
	close(fd);
}

/**
 * xdma_usr_event_wait - Blocking wait for event
 * @fd: File descriptor of the device
 * @event: Event num
 *
 */
ssize_t xdma_usr_event_wait(int fd, uint32_t *event)
{
	uint32_t value;
	ssize_t rc;

	rc = read(fd, &value, 4);
	if (rc == 4) {
		*event = value;
		printf("%s: event num %d\n", __func__, value); // testing
		return 0;
	}

	return rc;
}

/**
 * xdma_start_accel - Start accelerator
 * @map_base: Mapped base address of axilite master
 * @id: Accelerator ID
 *
 * Return: On success, return 0. Otherwise, return -ve value
 * Note: not thread-safe
 */
int xdma_start_accel(void *map_base, int id)
{
	void *reg_addr;
	uint32_t write_val, read_val;

	reg_addr = map_base + GPIO_DATA_REG;
	//printf("%s: reg_base %p\n", __func__, reg_addr);
	read_val = *((uint32_t *) reg_addr);
	write_val = read_val | (0x00000100 << id);
	//printf("%s: read_val 0x%x write_val 0x%x\n", __func__, read_val, write_val);
	*((uint32_t *) reg_addr) = write_val;
	usleep(1000);
	*((uint32_t *) reg_addr) = read_val;

	return 0;
}

/**
 * xdma_reset_accel - Reset accelerator
 * @map_base: Mapped base address of axilite master
 * @id: Accelerator ID
 *
 * Return: On success, return 0. Otherwise, return -ve value
 * Note: not thread-safe
 */
int xdma_reset_accel(void *map_base, int id)
{
	volatile void *reg_addr;
	uint32_t write_val, read_val, rst_bit;
	volatile uint32_t *bram_addr;

	/* clear accelerator ready status */
	bram_addr = map_base + BRAM_START_ADDR + ACCEL_RDY_OFS;
	*bram_addr = 0;

	/* assert reset */
	reg_addr = map_base + GPIO_DATA_REG;
	//printf("%s: reg_base %p\n", __func__, reg_addr);
	read_val = *((uint32_t *) reg_addr);
	rst_bit = (0x00000010 << id);
	write_val = read_val | rst_bit; // out of reset
	printf("%s: read_val 0x%x write_val 0x%x\n", __func__, read_val, write_val);
	printf("%s: start\n", __func__);
	*((volatile uint32_t *) reg_addr) = write_val;
	usleep(1000);
	write_val = read_val & ~rst_bit; // assert reset
	printf("%s: read_val 0x%x write_val 0x%x\n", __func__, read_val, write_val);
	*((volatile uint32_t *) reg_addr) = write_val;
	usleep(1000);
	write_val = read_val | rst_bit; // out of reset
	printf("%s: read_val 0x%x write_val 0x%x\n", __func__, read_val, write_val);
	*((volatile uint32_t *) reg_addr) = write_val;
	printf("%s: stop\n", __func__);
	return 0;
}

/**
 * xdma_set_params - Pass parameters to accelerator
 * @map_base: Mapped base address of axilite master
 * @src_addr_hi: Source address of data in devcie side (high 32bit)
 * @src_addr_lo: Source address of data in devcie side (low 32bit)
 * @dst_addr_hi: Source address of data in devcie side (high 32bit)
 * @dst_addr_lo: Source address of data in devcie side (low 32bit)
 * @size: Data size in byte
 * @cmd: Command
 * @param1: Parameters
 * @param2: Parameters
 *
 * Return: Always return 0
 * Note: not thread-safe
 */
int xdma_set_params(void *map_base, uint32_t src_addr_hi, \
	uint32_t src_addr_lo, uint32_t dst_addr_hi, uint32_t dst_addr_lo, \
	uint32_t size_hi, uint32_t size_lo, uint32_t cmd, \
	uint32_t param1, uint32_t param2)
{

	volatile uint32_t *bram_addr;

	bram_addr = map_base + BRAM_START_ADDR + ACCEL_SIG_OFS;
	//printf("%s: bram_addr %p\n", __func__, bram_addr);
	*bram_addr++ = 0xdeadbeef;
	*bram_addr++ = src_addr_hi;
	*bram_addr++ = src_addr_lo;
	*bram_addr++ = dst_addr_hi;
	*bram_addr++ = dst_addr_lo;
	*bram_addr++ = size_hi;
	*bram_addr++ = size_lo;
	*bram_addr++ = cmd;
	*bram_addr++ = param1;
	*bram_addr++ = param2;

	return 0;
}


int xdma_reg_write(void *map_base, uint32_t dst_addr, uint32_t data, int core)
{

	volatile uint32_t *addr;
	switch (core){
	case 0:
		addr = map_base + REG_RD_WR_ADDR_0 + dst_addr;
		break;
	case 1:
		addr = map_base + REG_RD_WR_ADDR_1 + dst_addr;
		break;
	case 2:
		addr = map_base + REG_RD_WR_ADDR_2 + dst_addr;
		break;
	case 3:
		addr = map_base + REG_RD_WR_ADDR_3 + dst_addr;
		break;
	case 4:
		addr = map_base + REG_RD_WR_ADDR_4 + dst_addr;
		break;
	default:
		addr = map_base + REG_RD_WR_ADDR_0 + dst_addr;
		break;
	}


	*addr = data;
	#ifdef XDMA_STATUS
		printf("%s: CORE %d, ADDR %p, DATA: %x\n", __func__, core, addr, data);
	#endif
	return 0;
}

int xdma_reg_read(void *map_base, uint32_t dst_addr, uint32_t *read_data, int core)
{

	volatile uint32_t *addr;

	switch (core){
	case 0:
		addr = map_base + REG_RD_WR_ADDR_0 + dst_addr;
		break;
	case 1:
		addr = map_base + REG_RD_WR_ADDR_1 + dst_addr;
		break;
	case 2:
		addr = map_base + REG_RD_WR_ADDR_2 + dst_addr;
		break;
	case 3:
		addr = map_base + REG_RD_WR_ADDR_3 + dst_addr;
		break;
	case 4:
		addr = map_base + REG_RD_WR_ADDR_4 + dst_addr;
		break;
	default:
		addr = map_base + REG_RD_WR_ADDR_0 + dst_addr;
		break;
	}

	read_data = *addr;
	#ifdef XDMA_STATUS
		printf("%s: CORE %d, ADDR %p, DATA: %x\n", __func__, core, addr, read_data);
	#endif
	return 0;
}

uint32_t xdma_reg_read_NoDump(void *map_base, uint32_t dst_addr, uint32_t *read_data, int core)
{

        volatile uint32_t *addr;

	switch (core){
	case 0:
		addr = map_base + REG_RD_WR_ADDR_0 + dst_addr;
		break;
	case 1:
		addr = map_base + REG_RD_WR_ADDR_1 + dst_addr;
		break;
	case 2:
		addr = map_base + REG_RD_WR_ADDR_2 + dst_addr;
		break;
	case 3:
		addr = map_base + REG_RD_WR_ADDR_3 + dst_addr;
		break;
	case 4:
		addr = map_base + REG_RD_WR_ADDR_4 + dst_addr;
		break;
	default:
		addr = map_base + REG_RD_WR_ADDR_0 + dst_addr;
		break;
	}

        read_data = *addr;

        return read_data;
}

/**
 * xdma_wait_accel_ready - Waiting accelerator ready
 * @map_base: Mapped base address of axilite master
 * @id: Accerlerator ID
 *
 * Return: Always return 0
 * Note: not thread-safe
 */
int xdma_wait_accel_ready(void *map_base, int id)
{
	volatile uint32_t *bram_addr;

	bram_addr = map_base + BRAM_START_ADDR + ACCEL_RDY_OFS;
	printf("%s: bram_addr %p\n", __func__, bram_addr);
	printf("%s: start\n", __func__);
	while (*bram_addr != 0xa5a5a5a5);
	printf("%s: stop\n", __func__);

	return 0;
}




// TODO - having using derivatives in header files is very poor style, and we need to fix these up.
//
// http://stackoverflow.com/questions/4872373/why-is-including-using-namespace-into-a-header-file-a-bad-idea-in-c
// 
// "However you'll virtually never see a using directive in a header file (at least not outside of scope).
// The reason is that using directive eliminate the protection of that particular namespace, and the effect last
// until the end of current compilation unit. If you put a using directive (outside of a scope) in a header file,
// it means that this loss of "namespace protection" will occur within any file that include this header,
// which often mean other header files."
//
// Bob has already done just that in https://github.com/bobsummerwill/cpp-ethereum/commits/cmake_fixes/ethminer,
// and we should apply those changes back to 'develop'.  It is probably best to defer that cleanup
// until after attempting to backport the Genoil ethminer changes, because they are fairly extensive
// in terms of lines of change, though all the changes are just adding explicit namespace prefixes.
// So let's start with just the subset of changes which minimizes the #include dependencies.
//
// See https://github.com/bobsummerwill/cpp-ethereum/commit/53af845268b91bc6aa1dab53a6eac675157a072b
// See https://github.com/bobsummerwill/cpp-ethereum/commit/3b9e581d7c04c637ebda18d3d86b5a24d29226f4
//
// More generally, the fact that nearly all of the code for ethminer is actually in the 'MinerAux.h'
// header file, rather than in a source file, is also poor style and should probably be addressed.
// Perhaps there is some historical reason for this which I am unaware of?

std::string data_element_short (std::string longstring, bool convert_endian)
{
	//data_element_short always process and return 4 bytes only. 
	//meant for endian conversion purpose.
	
        unsigned char size;
        unsigned char num_zero;
        size = longstring.length();
        if (size < 8)
        {
        	num_zero = 8 - size;
        	longstring.insert(longstring.begin(), num_zero, '0');
        }
        std::string data;
	std::string * data_;
	if (convert_endian == false)
       		data = longstring.substr(0, 8);
	else
		data = longstring.substr(6, 2) + longstring.substr(4, 2) + longstring.substr(2, 2) + longstring.substr(0, 2);
       
       	return data;
}





std::string * data_element (std::string longstring, unsigned char byte_num, bool convert_endian)
{
        unsigned char array_num;
        unsigned char i;
        unsigned char size;
        unsigned char num_zero;
        size = longstring.length();

        if (size < byte_num*2)
        {
        	num_zero = byte_num*2 - size;
        	longstring.insert(longstring.begin(), num_zero, '0');
        }

        if (byte_num <= 4)
        	array_num = 1;
        else if (byte_num <= 8)
        	array_num = 2;
        else if (byte_num <= 12)
            array_num = 3;
        else if (byte_num <= 16)
            array_num = 4;
        else if (byte_num <= 20)
            array_num = 5;
        else if (byte_num <= 24)
            array_num = 6;
        else if (byte_num <= 28)
            array_num = 7;
        else if (byte_num <= 32)
            array_num = 8;

        std::string * data = new std::string [array_num];
        for (i=0; i<array_num; i++)
        {
	   if (convert_endian == false)
           	data[i] = longstring.substr(8*i, 8);
	   else
	   {
		data[i] = longstring.substr(6+(8*i), 2) + longstring.substr(4+(8*i), 2) + longstring.substr(2+(8*i), 2) + longstring.substr(0+(8*i), 2);
	   }	   
           //cout << data[i] << endl;
        }
        return data;
}

std::string int_to_hex( uint64_t i )
{
  std::stringstream stream;
  stream << std::hex << i;
  return stream.str();
}

uint64_t hex_to_int ( std::string str )
{
	return std::stoull(str, 0, 16);
}

unsigned int get_file_size(std::string filename) // path to file
{
    FILE *p_file = NULL;
    p_file = fopen(filename.c_str(),"rb");
    fseek(p_file,0,SEEK_END);
    int size = ftell(p_file);
    fclose(p_file);
    return size;
}

void fpga_dram_read(uint64_t size, char *infname, uint64_t offset, uint64_t addr_h2c, uint64_t addr_c2h)
{
	int verbose = 1;
	uint64_t dma_chan = CHAN_DEFAULT;

	ssize_t rc;
	char *buffer_h2c = NULL;
	char *buffer_c2h = NULL;
	char *allocated_h2c = NULL;
	char *allocated_c2h = NULL;
	int infile_fd = -1;
	int dmah2c_fd = -1;
	int dmac2h_fd = -1;
	uint32_t *ptr2;
	uint32_t data1, data2;
	int err = 0;
	//int i;
	uint64_t i;

	/* Open DMA channel from card to host */
	dmac2h_fd = xdma_channel_open(dma_chan, DMA_DIR_C2H);
	if (dmac2h_fd < 0) {
		fprintf(stderr, "unable to open CHAN_C2H_%d, %d.\n",
			dma_chan, dmac2h_fd);
		perror("open device");
		rc = -EINVAL;
		goto out;
	}


	/* Allocate transfer buffers */
	/* c2h */
	posix_memalign((void **)&allocated_c2h, 4096 /*alignment */ , size + 4096);
	if (!allocated_c2h) {
		fprintf(stderr, "OOM %lu.\n", size + 4096);
		rc = -ENOMEM;
		goto out;
	}
	buffer_c2h = allocated_c2h + offset;
	if (verbose)
		fprintf(stdout, " host buffer  : %p (0x%lx)\n",
			buffer_c2h, size + 4096);

	memset(buffer_c2h, 0, size);

	printf("================= Start Transfer ===================\n");

	/* Start C2H transfer */
	if (verbose)
		fprintf(stdout, ">> Transfer data from accelerator_%d to host\n\n", ACCEL_DEFAULT);
	rc = xdma_c2h_transfer(dmac2h_fd, buffer_c2h, size, addr_c2h);
	if (rc < 0)
		goto out;

	/* Verify data */

	ptr2 = (uint32_t *)buffer_c2h;
	for (i=0; i<size/4; i++) {
		data2 = *ptr2;
		printf("0x%x (%p)\n", data2, ptr2);
		
		ptr2++;
	}

	if (verbose) {
		if (!err)
			fprintf(stdout, ">> Verification pass!!\n\n");
		printf("================ Process end ===================\n\n\n");
	}

	out:
	if (dmac2h_fd != -1)
		xdma_channel_close(dmac2h_fd);
	if (infile_fd >= 0)
		close(infile_fd);
	if (allocated_c2h)
		free(allocated_c2h);

}

void fpga_xdma_transfer_4cores(uint64_t size, char *infname, uint64_t offset, uint64_t addr_h2c, uint64_t addr_c2h)
{
	int verbose = 1;
	uint64_t dma_chan = CHAN_DEFAULT;

	ssize_t rc;
	char *buffer_h2c = NULL;
	char *buffer_c2h = NULL;
	char *allocated_h2c = NULL;
	char *allocated_c2h = NULL;
	int infile_fd = -1;
	int dmah2c_fd = -1;
	int dmac2h_fd = -1;
	uint32_t *ptr1, *ptr2;
	uint32_t data1, data2;
	int err = 0;
	//int i;
	uint64_t i;

	if (infname) {
		infile_fd = open(infname, O_RDONLY);
		if (infile_fd < 0) {
			fprintf(stderr, "unable to open input file %s, %d.\n",
				infname, infile_fd);
			perror("open input file");
			rc = -EINVAL;
			goto out;
		}
	}

	/* Open DMA channel from host to card */
	dmah2c_fd = xdma_channel_open(dma_chan, DMA_DIR_H2C);
	if (dmah2c_fd < 0) {
		fprintf(stderr, "unable to open CHAN_H2C_%d, %d.\n",
			dma_chan, dmah2c_fd);
		perror("open device");
		rc = -EINVAL;
		goto out;
	}

	/* Open DMA channel from card to host */
	dmac2h_fd = xdma_channel_open(dma_chan, DMA_DIR_C2H);
	if (dmac2h_fd < 0) {
		fprintf(stderr, "unable to open CHAN_C2H_%d, %d.\n",
			dma_chan, dmac2h_fd);
		perror("open device");
		rc = -EINVAL;
		goto out;
	}


	/* Allocate transfer buffers */
	/* h2c */
	posix_memalign((void **)&allocated_h2c, 4096 /*alignment */ , size + 4096);
	if (!allocated_h2c) {
		fprintf(stderr, "OOM %lu.\n", size + 4096);
		rc = -ENOMEM;
		goto out;
	}
	buffer_h2c = allocated_h2c + offset;
	if (verbose)
		fprintf(stdout, " host buffer  : %p (0x%lx)\n",
			buffer_h2c, size + 4096);

	if (infile_fd >= 0) {
		rc = read_to_buffer(infname, infile_fd, buffer_h2c, size, 0);
		if (rc < 0)
		{
			goto out;
		}
	}

	/* c2h */
	posix_memalign((void **)&allocated_c2h, 4096 /*alignment */ , size + 4096);
	if (!allocated_c2h) {
		fprintf(stderr, "OOM %lu.\n", size + 4096);
		rc = -ENOMEM;
		goto out;
	}
	buffer_c2h = allocated_c2h + offset;
	if (verbose)
		fprintf(stdout, " host buffer  : %p (0x%lx)\n",
			buffer_c2h, size + 4096);

	memset(buffer_c2h, 0, size);

	printf("================= Start Transfer ===================\n");

	/* Pass parameters to accelerator */
	//srand(time(NULL));
	//seed = (uint32_t)rand();
	//xdma_set_params(map_base, (uint32_t)(addr_h2c >> 32), \
		(uint32_t)(addr_h2c), (uint32_t)(addr_c2h >> 32), (uint32_t)(addr_c2h), \
		(uint32_t)(size >> 32), (uint32_t)(size), 0, seed, 0);

	/* Start H2C transfer */
	for (unsigned char k=1;k<=4;k++)
	{
		if (verbose)
			fprintf(stdout, ">> Transfer data from host to accelerator_%d\n\n", ACCEL_DEFAULT);
		rc = xdma_h2c_transfer(dmah2c_fd, buffer_h2c, size, addr_h2c*k);
		if (rc < 0)
			goto out;

		/* Start C2H transfer */
		if (verbose)
			fprintf(stdout, ">> Transfer data from accelerator_%d to host\n\n", ACCEL_DEFAULT);
		rc = xdma_c2h_transfer(dmac2h_fd, buffer_c2h, size, addr_c2h*k);
		if (rc < 0)
			goto out;

		/* Verify data */

		ptr1 = (uint32_t *)buffer_h2c;
		ptr2 = (uint32_t *)buffer_c2h;
		for (i=0; i<size/4; i++) {
			data1 = *ptr1;
			data2 = *ptr2;
			
			if (data2 != data1 /*+ seed*/) {
				printf("!!!! Data mismatch 0x%x (%p) 0x%x (%p)\n\n", data1, ptr1, data2, ptr2);
				err = 1;
				break;
			}
			
			ptr1++;
			ptr2++;
		}

		if (verbose) {
			if (!err)
				fprintf(stdout, ">> Verification pass!!\n\n");
			printf("================ Process end ===================\n\n\n");
		}
	

	}

	out:
	if (dmah2c_fd != -1)
		xdma_channel_close(dmah2c_fd);
	if (dmac2h_fd != -1)
		xdma_channel_close(dmac2h_fd);
	if (infile_fd >= 0)
		close(infile_fd);
	if (allocated_h2c)
		free(allocated_h2c);
	if (allocated_c2h)
		free(allocated_c2h);

}



void fpga_xdma_transfer(uint64_t size, char *infname, uint64_t offset, uint64_t addr_h2c, uint64_t addr_c2h)
{
	int verbose = 1;
	uint64_t dma_chan = CHAN_DEFAULT;

	ssize_t rc;
	char *buffer_h2c = NULL;
	char *buffer_c2h = NULL;
	char *allocated_h2c = NULL;
	char *allocated_c2h = NULL;
	int infile_fd = -1;
	int dmah2c_fd = -1;
	int dmac2h_fd = -1;
	uint32_t *ptr1, *ptr2;
	uint32_t data1, data2;
	int err = 0;
	//int i;
	uint64_t i;

	if (infname) {
		infile_fd = open(infname, O_RDONLY);
		if (infile_fd < 0) {
			fprintf(stderr, "unable to open input file %s, %d.\n",
				infname, infile_fd);
			perror("open input file");
			rc = -EINVAL;
			goto out;
		}
	}

	/* Open DMA channel from host to card */
	dmah2c_fd = xdma_channel_open(dma_chan, DMA_DIR_H2C);
	if (dmah2c_fd < 0) {
		fprintf(stderr, "unable to open CHAN_H2C_%d, %d.\n",
			dma_chan, dmah2c_fd);
		perror("open device");
		rc = -EINVAL;
		goto out;
	}

	/* Open DMA channel from card to host */
	dmac2h_fd = xdma_channel_open(dma_chan, DMA_DIR_C2H);
	if (dmac2h_fd < 0) {
		fprintf(stderr, "unable to open CHAN_C2H_%d, %d.\n",
			dma_chan, dmac2h_fd);
		perror("open device");
		rc = -EINVAL;
		goto out;
	}


	/* Allocate transfer buffers */
	/* h2c */
	posix_memalign((void **)&allocated_h2c, 4096 /*alignment */ , size + 4096);
	if (!allocated_h2c) {
		fprintf(stderr, "OOM %lu.\n", size + 4096);
		rc = -ENOMEM;
		goto out;
	}
	buffer_h2c = allocated_h2c + offset;
	if (verbose)
		fprintf(stdout, " host buffer  : %p (0x%lx)\n",
			buffer_h2c, size + 4096);

	if (infile_fd >= 0) {
		rc = read_to_buffer(infname, infile_fd, buffer_h2c, size, 0);
		if (rc < 0)
		{
			goto out;
		}
	}

	/* c2h */
	posix_memalign((void **)&allocated_c2h, 4096 /*alignment */ , size + 4096);
	if (!allocated_c2h) {
		fprintf(stderr, "OOM %lu.\n", size + 4096);
		rc = -ENOMEM;
		goto out;
	}
	buffer_c2h = allocated_c2h + offset;
	if (verbose)
		fprintf(stdout, " host buffer  : %p (0x%lx)\n",
			buffer_c2h, size + 4096);

	memset(buffer_c2h, 0, size);

	printf("================= Start Transfer ===================\n");

	/* Pass parameters to accelerator */
	//srand(time(NULL));
	//seed = (uint32_t)rand();
	//xdma_set_params(map_base, (uint32_t)(addr_h2c >> 32), \
		(uint32_t)(addr_h2c), (uint32_t)(addr_c2h >> 32), (uint32_t)(addr_c2h), \
		(uint32_t)(size >> 32), (uint32_t)(size), 0, seed, 0);

	/* Start H2C transfer */
	if (verbose)
		fprintf(stdout, ">> Transfer data from host to accelerator_%d\n\n", ACCEL_DEFAULT);
	rc = xdma_h2c_transfer(dmah2c_fd, buffer_h2c, size, addr_h2c);
	if (rc < 0)
		goto out;

	/* Start C2H transfer */
	if (verbose)
		fprintf(stdout, ">> Transfer data from accelerator_%d to host\n\n", ACCEL_DEFAULT);
	rc = xdma_c2h_transfer(dmac2h_fd, buffer_c2h, size, addr_c2h);
	if (rc < 0)
		goto out;

	/* Verify data */

	ptr1 = (uint32_t *)buffer_h2c;
	ptr2 = (uint32_t *)buffer_c2h;
	for (i=0; i<size/4; i++) {
		data1 = *ptr1;
		data2 = *ptr2;
		
		if (data2 != data1 /*+ seed*/) {
			printf("!!!! Data mismatch 0x%x (%p) 0x%x (%p)\n\n", data1, ptr1, data2, ptr2);
			err = 1;
			break;
		}
		
		ptr1++;
		ptr2++;
	}

	if (verbose) {
		if (!err)
			fprintf(stdout, ">> Verification pass!!\n\n");
		printf("================ Process end ===================\n\n\n");
	}

	out:
	if (dmah2c_fd != -1)
		xdma_channel_close(dmah2c_fd);
	if (dmac2h_fd != -1)
		xdma_channel_close(dmac2h_fd);
	if (infile_fd >= 0)
		close(infile_fd);
	if (allocated_h2c)
		free(allocated_h2c);
	if (allocated_c2h)
		free(allocated_c2h);

}

void fpga_memwrite(std::string longstring, unsigned int byte_num, uint32_t addr, int core) {
        
	//std::cout << longstring << std::endl;

        std::string * header = data_element(longstring, byte_num, false);
	int axilite_master_fd = -1;
        size_t map_size = AXILITE_MASTER_MAP_SIZE;
        void *map_base = NULL;
        unsigned int array_num;
        unsigned char i;
        uint32_t reg;

	/* Open axilite master device */
        axilite_master_fd = xdma_axilite_master_open(0);
        if (axilite_master_fd < 0) {
                fprintf(stderr, "unable to open AXILITE_MASTER_%d, %d.\n",
                        0, axilite_master_fd);
                perror("open device");
                goto out;
        }

        xdma_axilite_master_mmap(axilite_master_fd, map_size, &map_base, 0);

        if (byte_num <= 4)
        	array_num = 1;
        else if (byte_num <= 8)
        	array_num = 2;
        else if (byte_num <= 12)
            array_num = 3;
        else if (byte_num <= 16)
            array_num = 4;
        else if (byte_num <= 20)
            array_num = 5;
        else if (byte_num <= 24)
            array_num = 6;
        else if (byte_num <= 28)
            array_num = 7;
        else if (byte_num <= 32)
            array_num = 8;

        for (i=0; i<array_num; i++)
        {
        		uint32_t data;
                std::stringstream ss;
                ss << std::hex << header[(array_num -1) - i]; //LSB write first. MSB write last
                ss >> data;
                //cout << data << endl;
                reg = 4*i;
		xdma_reg_write(map_base, addr+reg, data, core);
                //dma_pcie(1, data, addr + reg);
        }

	out:
                if (map_base)
                        xdma_axilite_master_mumap(map_base, map_size);
                if (axilite_master_fd != -1)
                        xdma_axilite_master_close(axilite_master_fd);

}

void fpga_memread(uint32_t addr, int core)
{

	int axilite_master_fd = -1;
	size_t map_size = AXILITE_MASTER_MAP_SIZE;
	void *map_base = NULL;
	uint32_t reg_read_data;
	uint32_t read_result;

	/* Open axilite master device */
	axilite_master_fd = xdma_axilite_master_open(0);
	if (axilite_master_fd < 0) {
		fprintf(stderr, "unable to open AXILITE_MASTER_%d, %d.\n",
			0, axilite_master_fd);
		perror("open device");
		goto out;
	}

	xdma_axilite_master_mmap(axilite_master_fd, map_size, &map_base, 0);
	//printf("mapbase %x \n\n", map_base);
	/* Open file for test data */

	xdma_reg_read(map_base, addr, &reg_read_data, core);


	out:
		if (map_base)
			xdma_axilite_master_mumap(map_base, map_size);
		if (axilite_master_fd != -1)
			xdma_axilite_master_close(axilite_master_fd);

		return read_result;
}

std::string fpga_memread_return(uint32_t addr, bool convert_endian, int core)
{
	std::string data; // = data_element(longstring, byte_num);
	int axilite_master_fd = -1;
	size_t map_size = AXILITE_MASTER_MAP_SIZE;
	void *map_base = NULL;
	uint32_t reg_read_data;
	uint32_t read_result;
	std::stringstream ss;
    
	/* Open axilite master device */
	axilite_master_fd = xdma_axilite_master_open(0);
	if (axilite_master_fd < 0) {
		fprintf(stderr, "unable to open AXILITE_MASTER_%d, %d.\n",
			0, axilite_master_fd);
		perror("open device");
		goto out;
	}

	xdma_axilite_master_mmap(axilite_master_fd, map_size, &map_base, 0);
	//printf("mapbase %x \n\n", map_base);
	/* Open file for test data */

	read_result = xdma_reg_read_NoDump(map_base, addr, &reg_read_data, core);
	ss << std::hex << read_result;
	data = data_element_short(ss.str(), convert_endian);


	out:
		if (map_base)
			xdma_axilite_master_mumap(map_base, map_size);
		if (axilite_master_fd != -1)
			xdma_axilite_master_close(axilite_master_fd);

		return data;
}

double * Hashrate(double arr[], int core)
{
	int axilite_master_fd = -1;
	size_t map_size = AXILITE_MASTER_MAP_SIZE;
	void *map_base = NULL;
	uint32_t reg_read_data;
	//uint32_t hash_cnt;
	//uint32_t clk_cnt;
	double clk_freq = 250.00 * 1000000; //250MHz
	double Hashrate_;
    
	/* Open axilite master device */
	axilite_master_fd = xdma_axilite_master_open(0);
	if (axilite_master_fd < 0) {
		fprintf(stderr, "unable to open AXILITE_MASTER_%d, %d.\n",
			0, axilite_master_fd);
		perror("open device");
		goto out;
	}

	xdma_axilite_master_mmap(axilite_master_fd, map_size, &map_base, 0);
	//printf("mapbase %x \n\n", map_base);
	/* Open file for test data */
	double hash_cnt;
	double clk_cnt;
	double time;
        hash_cnt = xdma_reg_read_NoDump(map_base, 0x0000006C, &reg_read_data, core);
        clk_cnt	= xdma_reg_read_NoDump(map_base, 0x00000070, &reg_read_data, core);
       	time = clk_cnt * ( 1 / clk_freq );

	Hashrate_ = double(hash_cnt/time);
	//std::cout << "FPGA: " << uint32_t(Hashrate_) << std::endl;
	//std::cout << uint32_t(hash_cnt) << std::endl;
	//std::cout << uint32_t(clk_cnt) << std::endl;
	//std::cout << std::setprecision(5) << time << std::endl;

	out:
		if (map_base)
			xdma_axilite_master_mumap(map_base, map_size);
		if (axilite_master_fd != -1)
			xdma_axilite_master_close(axilite_master_fd);

	arr[0] = Hashrate_;
	arr[1] = hash_cnt;
	arr[2] = clk_cnt;
	arr[3] = time;

	return arr; 
}	

void Set_StartNonce(std::string strtnonce, int core_select)
{ 
	uint64_t strtnonce_int = hex_to_int(strtnonce);
	std::cout << strtnonce_int + 1 << std::endl;
	std::string strtnonce_str = int_to_hex(strtnonce_int + 1);
	std::cout << strtnonce_str << std::endl;
	fpga_memwrite(strtnonce_str, 8, 0x20, core_select); 	 //innounce[63:0]

	std::cout << fpga_memread_return(0x20, false, core_select) << "innounce[31:0]" << std::endl;
	std::cout << fpga_memread_return(0x24, false, core_select) << "innounce[63:32]" << std::endl;
}

void Start_Core(int core_select)
{
	fpga_memwrite("00000001", 4, 0x30, core_select);  //reset deassert
	fpga_memwrite("00000003", 4, 0x30, core_select);  //start         

}
void Reset_Core(int select)
{
	switch (select)
	{
		case 255:
			fpga_memwrite("00000000", 4, 0x30, 0); 	//reset in intr, core 0
			fpga_memwrite("00000000", 4, 0x30, 1); 	//reset in intr, core 1
			fpga_memwrite("00000000", 4, 0x30, 2); 	//reset in intr, core 2
			fpga_memwrite("00000000", 4, 0x30, 3); 	//reset in intr, core 3
			fpga_memwrite("00000000", 4, 0x30, 4); 	//reset in intr, daggen
		case 0:
			fpga_memwrite("00000000", 4, 0x30, 0); 	//reset in intr, core 0
		case 1:
			fpga_memwrite("00000000", 4, 0x30, 1); 	//reset in intr, core 1
		case 2:
			fpga_memwrite("00000000", 4, 0x30, 2); 	//reset in intr, core 2
		case 3:
			fpga_memwrite("00000000", 4, 0x30, 3); 	//reset in intr, core 3
		case 4:
			fpga_memwrite("00000000", 4, 0x30, 4); 	//reset in intr, core 4
		default:
			break;
	}

}

void Write_SelfTest(int core_select, bool dram)
{
	cnote <<" SelfTest: "<< (dram? "Fixed DRAM" : "Hardcode RTL") << "  Core: " << core_select;
	fpga_memwrite("00000000", 4, 0x30, core_select);
	fpga_memwrite("a353f77e", 4, 0x00, core_select);
	fpga_memwrite("858ebf4b", 4, 0x04, core_select);
	fpga_memwrite("320df9dc", 4, 0x08, core_select);
	fpga_memwrite("f030e3c9", 4, 0x0C, core_select);
	fpga_memwrite("7a622ee1", 4, 0x10, core_select);
	fpga_memwrite("021d68ac", 4, 0x14, core_select);
	fpga_memwrite("1e58d12c", 4, 0x18, core_select);
	fpga_memwrite("dee1879a", 4, 0x1C, core_select);
	fpga_memwrite("35F59915", 4, 0x20, core_select);
	fpga_memwrite("9C5E48E9", 4, 0x24, core_select);
	fpga_memwrite("0000510F", 4, 0x28, core_select);
	fpga_memwrite("0193FFFF", 4, 0x2C, core_select);
	fpga_memwrite("f5F59916", 4, 0x34, core_select);
	fpga_memwrite("9C5E48E9", 4, 0x38, core_select);
	fpga_memwrite("FFFFFFFF", 4, 0x80, core_select);
	fpga_memwrite("FFFFFFFF", 4, 0x84, core_select);

	if (dram == false)
	{
		fpga_memwrite("00010000", 4, 0x30, core_select);
		fpga_memwrite("00010001", 4, 0x30, core_select);
		fpga_memwrite("00010003", 4, 0x30, core_select);
	}
	else
	{	
		fpga_memwrite("00000001", 4, 0x30, core_select);
		fpga_memwrite("00000003", 4, 0x30, core_select);
	}

}

void Read_SelfTest(int core_select)
{
	bool Result = false;
	/* 
	cnote << fpga_memread_return(0x00000000a0, false, core_select);
        cnote << fpga_memread_return(0x00000000a4, false, core_select);
        cnote << fpga_memread_return(0x00000000a8, false, core_select);
        cnote << fpga_memread_return(0x00000000ac, false, core_select);
        cnote << fpga_memread_return(0x00000000b0, false, core_select);
        cnote << fpga_memread_return(0x00000000b4, false, core_select);
        cnote << fpga_memread_return(0x00000000b8, false, core_select);
        cnote << fpga_memread_return(0x00000000bc, false, core_select);
        cnote << fpga_memread_return(0x0000000060, false, core_select);
        cnote << fpga_memread_return(0x0000000064, false, core_select);
        cnote << fpga_memread_return(0x0000000068, false, core_select);
	*/
	

	Result = (fpga_memread_return(0x00000000a0, false, core_select) == "9649b1cc")?(
		 (fpga_memread_return(0x00000000a4, false, core_select) == "78416422")?(
		 (fpga_memread_return(0x00000000a8, false, core_select) == "71849e27")?(
		 (fpga_memread_return(0x00000000ac, false, core_select) == "e6ece753")?(
		 (fpga_memread_return(0x00000000b0, false, core_select) == "cb81fcf3")?(
		 (fpga_memread_return(0x00000000b4, false, core_select) == "d3614c24")?(
		 (fpga_memread_return(0x00000000b8, false, core_select) == "6cafe9f0")?(
		 (fpga_memread_return(0x00000000bc, false, core_select) == "3afcdde5")?( 
		 (fpga_memread_return(0x0000000060, false, core_select) == "35f59915")?(
		 (fpga_memread_return(0x0000000064, false, core_select) == "9c5e48e9")?true:false) :false) :false) :false) :false) :false) :false) :false) :false) :false;

	if (Result == true)
		cnote << "Core: " << core_select << " SelfTest PASS!";
	else
		cnote << "Core: " << core_select << " BACKTEST ERROR....RTL not right!";

}

void LoopSelfTest()
{
	cnote <<"Write SelfTest Registers";
	Write_SelfTest(0, false);
	Write_SelfTest(1, false);
	Write_SelfTest(2, false);
	Write_SelfTest(3, false);

	cnote << "Read Hashing Result!";
	Read_SelfTest(0);
	Read_SelfTest(1);
	Read_SelfTest(2);
	Read_SelfTest(3);

	Reset_Core(255);
	cnote <<"Program DRAM with Fixed Pattern ";
	fpga_xdma_transfer_4cores(3388997504, "./YH_Input.bin", 0 ,0x400000000 ,0x400000000);
        //fpga_xdma_transfer(3388997504, "./YH_Input.bin", 0 ,0x800000000 ,0x800000000);
        //fpga_xdma_transfer(3388997504, "./YH_Input.bin", 0 ,0xc00000000 ,0xc00000000);
        //fpga_xdma_transfer(3388997504, "./YH_Input.bin", 0 ,0x1000000000 ,0x1000000000);
	
	cnote <<"Write SelfTest Registers";
	Write_SelfTest(0, true);
	Write_SelfTest(1, true);
	Write_SelfTest(2, true);
	Write_SelfTest(3, true);

	cnote << "Read Hashing Result!";
	Read_SelfTest(0);
	Read_SelfTest(1);
	Read_SelfTest(2);
	Read_SelfTest(3);



}

std::string intr_status(int core_select)
{
	std::string status;
	status = fpga_memread_return(0x68, false, core_select);
	//std::cout << "intr status" << fpga_memread_return(0x68, false, 0) << std::endl;
	//std::cout << "debug/start/reset bit" << fpga_memread_return(0x00000030, false, core_select) << std::endl;

	return status;
}


using namespace std;
using namespace dev;
using namespace dev::eth;


bool isTrue(std::string const& _m)
{
	return _m == "on" || _m == "yes" || _m == "true" || _m == "1";
}

bool isFalse(std::string const& _m)
{
	return _m == "off" || _m == "no" || _m == "false" || _m == "0";
}

inline std::string credits()
{
	std::ostringstream out;
	out
		<< "cpp-ethereum " << dev::Version << endl
		<< "  By cpp-ethereum contributors, (c) 2013-2016." << endl
		<< "  See the README for contributors and credits." << endl;
	return out.str();

}

class BadArgument: public Exception {};
struct MiningChannel: public LogChannel
{
	static const char* name() { return EthGreen "miner"; }
	static const int verbosity = 2;
	static const bool debug = false;
};
#define minelog clog(MiningChannel)

class MinerCLI
{
public:
	
	string compare[2];
	string out_nonce[2];
	string out_outhash2[8];
	string out_headerhash[8];
	string out_mixhash[8];

	void Read_Outhash(int core_select)
	{
	//printf("debug/start/reset bit=\n");
	//std::cout << "debug/start/reset bit" << fpga_memread_return(0x00000030, false, core_select) << std::endl;
        //printf("\nouthash2=\n");
	out_headerhash[0] = fpga_memread_return(0x00000000,true, core_select);
	out_headerhash[1] = fpga_memread_return(0x00000004,true, core_select); 
	out_headerhash[2] = fpga_memread_return(0x00000008,true, core_select); 
	out_headerhash[3] = fpga_memread_return(0x0000000c,true, core_select); 
	out_headerhash[4] = fpga_memread_return(0x00000010,true, core_select); 
	out_headerhash[5] = fpga_memread_return(0x00000014,true, core_select); 
	out_headerhash[6] = fpga_memread_return(0x00000018,true, core_select); 
	out_headerhash[7] = fpga_memread_return(0x0000001c,true, core_select); 
	out_mixhash[0] = fpga_memread_return(0x000000c0,true, core_select);
        out_mixhash[1] = fpga_memread_return(0x000000c4,true, core_select); 
        out_mixhash[2] = fpga_memread_return(0x000000c8,true, core_select); 
        out_mixhash[3] = fpga_memread_return(0x000000cc,true, core_select); 
        out_mixhash[4] = fpga_memread_return(0x000000d0,true, core_select); 
        out_mixhash[5] = fpga_memread_return(0x000000d4,true, core_select); 
        out_mixhash[6] = fpga_memread_return(0x000000d8,true, core_select); 
        out_mixhash[7] = fpga_memread_return(0x000000dc,true, core_select); 
	out_outhash2[0] = fpga_memread_return(0x000000A0,false, core_select);
        out_outhash2[1] = fpga_memread_return(0x000000A4,false, core_select);
        out_outhash2[2] = fpga_memread_return(0x000000A8,false, core_select);
        out_outhash2[3] = fpga_memread_return(0x000000Ac,false, core_select);
        out_outhash2[4] = fpga_memread_return(0x000000B0,false, core_select);
	out_outhash2[5] = fpga_memread_return(0x000000B4,false, core_select);
        out_outhash2[6] = fpga_memread_return(0x000000B8,false, core_select);
        out_outhash2[7] = fpga_memread_return(0x000000Bc,false, core_select);
        //printf("\noutnonce=\n");
	out_nonce[0] = fpga_memread_return(0x00000060,false, core_select);
        out_nonce[1] = fpga_memread_return(0x00000064,false, core_select);

        //printf("\ninterrupt=\n");
	//std::cout << "interrupt" << fpga_memread_return(0x00000068, false, core_select) << std::endl;
//
	//printf("compare=\n");
   	compare[0] = fpga_memread_return(0x00000080,false, core_select);
        compare[1] = fpga_memread_return(0x00000084,false, core_select);
}

	enum class OperationMode
	{
		None,
		DAGInit,
		Benchmark,
		Farm
	};


	MinerCLI(OperationMode _mode = OperationMode::None): mode(_mode) {
		Ethash::init();
		NoProof::init();
		BasicAuthority::init();
	}

	bool interpretOption(int& i, int argc, char** argv)
	{
		string arg = argv[i];
		if ((arg == "-F" || arg == "--farm") && i + 1 < argc)
		{
			mode = OperationMode::Farm;
			m_farmURL = argv[++i];
		}
		else if (arg == "--farm-recheck" && i + 1 < argc)
			try {
				m_farmRecheckPeriod = stol(argv[++i]);
			}
			catch (...)
			{
				cerr << "Bad " << arg << " option: " << argv[i] << endl;
				BOOST_THROW_EXCEPTION(BadArgument());
			}
		else if (arg == "--benchmark-warmup" && i + 1 < argc)
			try {
				m_benchmarkWarmup = stol(argv[++i]);
			}
			catch (...)
			{
				cerr << "Bad " << arg << " option: " << argv[i] << endl;
				BOOST_THROW_EXCEPTION(BadArgument());
			}
		else if (arg == "--benchmark-trial" && i + 1 < argc)
			try {
				m_benchmarkTrial = stol(argv[++i]);
			}
			catch (...)
			{
				cerr << "Bad " << arg << " option: " << argv[i] << endl;
				BOOST_THROW_EXCEPTION(BadArgument());
			}
		else if (arg == "--benchmark-trials" && i + 1 < argc)
			try {
				m_benchmarkTrials = stol(argv[++i]);
			}
			catch (...)
			{
				cerr << "Bad " << arg << " option: " << argv[i] << endl;
				BOOST_THROW_EXCEPTION(BadArgument());
			}
		else if (arg == "-C" || arg == "--cpu")
			m_minerType = "cpu";
		else if (arg == "--current-block" && i + 1 < argc)
			m_currentBlock = stol(argv[++i]);
		else if (arg == "--no-precompute")
		{
			m_precompute = false;
		}
		else if ((arg == "-D" || arg == "--create-dag") && i + 1 < argc)
		{
			string m = boost::to_lower_copy(string(argv[++i]));
			mode = OperationMode::DAGInit;
			try
			{
				m_initDAG = stol(m);
			}
			catch (...)
			{
				cerr << "Bad " << arg << " option: " << m << endl;
				BOOST_THROW_EXCEPTION(BadArgument());
			}
		}
		else if ((arg == "-w" || arg == "--check-pow") && i + 4 < argc)
		{
			string m;
			try
			{
				BlockHeader bi;
				m = boost::to_lower_copy(string(argv[++i]));
				h256 powHash(m);
				m = boost::to_lower_copy(string(argv[++i]));
				h256 seedHash;
				if (m.size() == 64 || m.size() == 66)
					seedHash = h256(m);
				else
					seedHash = EthashAux::seedHash(stol(m));
				m = boost::to_lower_copy(string(argv[++i]));
				bi.setDifficulty(u256(m));
				auto boundary = Ethash::boundary(bi);
				m = boost::to_lower_copy(string(argv[++i]));
				Ethash::setNonce(bi, h64(m));
				auto r = EthashAux::eval(seedHash, powHash, h64(m));
				bool valid = r.value < boundary;
				cout << (valid ? "VALID :-)" : "INVALID :-(") << endl;
				cout << r.value << (valid ? " < " : " >= ") << boundary << endl;
				cout << "  where " << boundary << " = 2^256 / " << bi.difficulty() << endl;
				cout << "  and " << r.value << " = ethash(" << powHash << ", " << h64(m) << ")" << endl;
				cout << "  with seed as " << seedHash << endl;
				if (valid)
					cout << "(mixHash = " << r.mixHash << ")" << endl;
				cout << "SHA3( light(seed) ) = " << sha3(EthashAux::light(Ethash::seedHash(bi))->data()) << endl;
				exit(0);
			}
			catch (...)
			{
				cerr << "Bad " << arg << " option: " << m << endl;
				BOOST_THROW_EXCEPTION(BadArgument());
			}
		}
		else if (arg == "-M" || arg == "--benchmark")
			mode = OperationMode::Benchmark;
		else if ((arg == "-t" || arg == "--mining-threads") && i + 1 < argc)
		{
			try {
				m_miningThreads = stol(argv[++i]);
			}
			catch (...)
			{
				cerr << "Bad " << arg << " option: " << argv[i] << endl;
				BOOST_THROW_EXCEPTION(BadArgument());
			}
		}
                else if (arg == "--disable-submit-hashrate")
                        m_submitHashrate = false;
		else
			return false;
		return true;
	}

	void execute()
	{
		if (m_minerType == "cpu")
			EthashCPUMiner::setNumInstances(m_miningThreads);
		if (mode == OperationMode::DAGInit)
			doInitDAG(m_initDAG);
		else if (mode == OperationMode::Benchmark)
			doBenchmark(m_minerType, m_benchmarkWarmup, m_benchmarkTrial, m_benchmarkTrials);
		else if (mode == OperationMode::Farm)
			doFarm(m_minerType, m_farmURL, m_farmRecheckPeriod);
	}

	static void streamHelp(ostream& _out)
	{
		_out
			<< "Work farming mode:" << endl
			<< "    -F,--farm <url>  Put into mining farm mode with the work server at URL (default: http://127.0.0.1:8545)" << endl
			<< "    --farm-recheck <n>  Leave n ms between checks for changed work (default: 500)." << endl
			<< "    --no-precompute  Don't precompute the next epoch's DAG." << endl
			<< "Ethash verify mode:" << endl
			<< "    -w,--check-pow <headerHash> <seedHash> <difficulty> <nonce>  Check PoW credentials for validity." << endl
			<< endl
			<< "Benchmarking mode:" << endl
			<< "    -M,--benchmark  Benchmark for mining and exit; use with --cpu and --opencl." << endl
			<< "    --benchmark-warmup <seconds>  Set the duration of warmup for the benchmark tests (default: 3)." << endl
			<< "    --benchmark-trial <seconds>  Set the duration for each trial for the benchmark tests (default: 3)." << endl
			<< "    --benchmark-trials <n>  Set the number of trials for the benchmark tests (default: 5)." << endl
			<< "DAG creation mode:" << endl
			<< "    -D,--create-dag <number>  Create the DAG in preparation for mining on given block and exit." << endl
			<< "Mining configuration:" << endl
			<< "    -C,--cpu  When mining, use the CPU." << endl
			<< "    -t, --mining-threads <n> Limit number of CPU/GPU miners to n (default: use everything available on selected platform)" << endl
			<< "    --current-block Let the miner know the current block number at configuration time. Will help determine DAG size and required GPU memory." << endl
			<< "    --disable-submit-hashrate  When mining, don't submit hashrate to node." << endl;
	}

	std::string minerType() const { return m_minerType; }
	bool shouldPrecompute() const { return m_precompute; }

private:
	void doInitDAG(unsigned _n)
	{
		h256 seedHash = EthashAux::seedHash(_n);
		cout << "Initializing DAG for epoch beginning #" << (_n / 30000 * 30000) << " (seedhash " << seedHash.abridged() << "). This will take a while." << endl;
		EthashAux::full(seedHash, true);
		exit(0);
	}

	void doBenchmark(std::string _m, unsigned _warmupDuration = 15, unsigned _trialDuration = 3, unsigned _trials = 5)
	{
		BlockHeader genesis;
		genesis.setDifficulty(1 << 18);
		cdebug << Ethash::boundary(genesis);

		GenericFarm<EthashProofOfWork> f;
		map<string, GenericFarm<EthashProofOfWork>::SealerDescriptor> sealers;
		sealers["cpu"] = GenericFarm<EthashProofOfWork>::SealerDescriptor{&EthashCPUMiner::instances, [](GenericMiner<EthashProofOfWork>::ConstructionInfo ci){ return new EthashCPUMiner(ci); }};
		f.setSealers(sealers);
		f.onSolutionFound([&](EthashProofOfWork::Solution) { return false; });

		string platformInfo = EthashCPUMiner::platformInfo();
		cout << "Benchmarking on platform: " << platformInfo << endl;

		cout << "Preparing DAG..." << endl;
		Ethash::ensurePrecomputed(0);

		genesis.setDifficulty(u256(1) << 63);
		f.setWork(genesis);
		f.start(_m);

		map<u256, WorkingProgress> results;
		u256 mean = 0;
		u256 innerMean = 0;
		for (unsigned i = 0; i <= _trials; ++i)
		{
			if (!i)
				cout << "Warming up..." << endl;
			else
				cout << "Trial " << i << "... " << flush;
			this_thread::sleep_for(chrono::seconds(i ? _trialDuration : _warmupDuration));

			auto mp = f.miningProgress();
			f.resetMiningProgress();
			if (!i)
				continue;
			auto rate = mp.rate();

			cout << rate << endl;
			results[rate] = mp;
			mean += rate;
		}
		f.stop();
		int j = -1;
		for (auto const& r: results)
			if (++j > 0 && j < (int)_trials - 1)
				innerMean += r.second.rate();
		innerMean /= (_trials - 2);
		cout << "min/mean/max: " << results.begin()->second.rate() << "/" << (mean / _trials) << "/" << results.rbegin()->second.rate() << " H/s" << endl;
		cout << "inner mean: " << innerMean << " H/s" << endl;
		exit(0);
	}

	// dummy struct for special exception.
	struct NoWork {};
	void doFarm(std::string _m, string const& _remote, unsigned _recheckPeriod)
	{
		/*    num_pages     */
		std::stringstream np;
	    //	uint32_t val_num_pages = 26476543;  //hardcode num page
	    //  uint32_t val_num_pages = 26804203; //26738687; //26673149; //26738688;//3422551944; //26476543;

	    //  np << std::hex << val_num_pages;
	    //  std::string num_page ( np.str() );
	        std::string num_page;

		/*    factor        */
		std::stringstream fac;
		//uint32_t val_factor = uint32_t(1<<31) / val_num_pages;
		//fac << std::hex << val_factor;
		//std::string factor ( fac.str() );
		std::string factor;

		/*    startnonce    */
		std::string startnonce[4] = { "0000000000000000", "4000000000000000", "8000000000000000", "c000000000000000" };


		/*    stopnonce    */
		//std::string stopnonce ( "FFFFFFFFFFFFFFFF" );
		std::string stopnonce[4] = { "3FFFFFFFFFFFFFFF", "7FFFFFFFFFFFFFFF", "BFFFFFFFFFFFFFFF", "FFFFFFFFFFFFFFFF" };

		/*    inhash       */
		// Being set below //
		//std::string keccak_input_hash = w.header.hex();
		//std::string keccak_input_hash = "dee1879a1e58d12c021d68ac7a622ee1f030e3c9320df9dc858ebf4ba353f77e";

		/*    compare      */
		// Being set below //
		//cc<< std::hex << w.boundary; // int decimal_value
		
		std::string DAG_FileName;
		uint32_t *data32, *value32;
		uint8_t *DAGName;
		unsigned int DAG_FileSize;
		uint32_t val_num_pages;

		uint64_t tryNonce = 0;

		map<string, GenericFarm<EthashProofOfWork>::SealerDescriptor> sealers;
		sealers["cpu"] = GenericFarm<EthashProofOfWork>::SealerDescriptor{&EthashCPUMiner::instances, [](GenericMiner<EthashProofOfWork>::ConstructionInfo ci){ return new EthashCPUMiner(ci); }};
		(void)_m;
		(void)_remote;
		(void)_recheckPeriod;
		jsonrpc::HttpClient client(_remote);

		h256 id = h256::random();
		::FarmClient rpc(client);
		GenericFarm<EthashProofOfWork> f;
		f.setSealers(sealers);
		f.start(_m);

		EthashProofOfWork::WorkPackage current;
		EthashAux::FullType dag;
		unsigned epoch;
		uint64_t blockNumber;
		uint64_t dag_nodes;
		uint64_t lightcache_nodes;
		uint64_t lightcache_base_index;
                uint64_t dag_base_index;
		uint32_t factor_dag;

		Reset_Core(255);
		LoopSelfTest();

		


		
		while (true)
			try
			{
				bool completed = false;
				auto start = std::chrono::system_clock::now();
				EthashProofOfWork::Solution solution;
				f.onSolutionFound([&](EthashProofOfWork::Solution sol)
				{
					solution = sol;
					completed = true;
					return true;
				});
				
				Reset_Core(255);
				while (!completed && (intr_status(0)=="00000000"))
				//while (!completed && (intr_status(0)!="00000001") && (intr_status(1)!="00000001") && (intr_status(2)!="00000001") && (intr_status(3)!="00000001"))
				//while (!completed)
				{
					auto mp = f.miningProgress();
					f.resetMiningProgress();
					if (current)
					{

					      double arr0[4];
					      double arr1[4];
					      double arr2[4];
					      double arr3[4];
					      double * result0 = Hashrate(arr0, 0);
					      double * result1 = Hashrate(arr1, 1);
					      double * result2 = Hashrate(arr2, 2);
					      double * result3 = Hashrate(arr3, 3);
					      #ifdef SW_Mining_Only
						minelog << "Mining on PoWhash" << current.headerHash << ": SW: " << mp;
					      #elif FPGA_Mining_Only
						minelog << "Mining on PoWhash" << current.headerHash;
						minelog << "   FPGA_core0: " << result0[0] << "H/s = " << uint32_t(result0[1]) << "hashes / " << result0[3] << "s (" << uint32_t(result0[2]) << "clk)";
						minelog << "   FPGA_core1: " << result1[0] << "H/s = " << uint32_t(result1[1]) << "hashes / " << result1[3] << "s (" << uint32_t(result1[2]) << "clk)";
						minelog << "   FPGA_core2: " << result2[0] << "H/s = " << uint32_t(result2[1]) << "hashes / " << result2[3] << "s (" << uint32_t(result2[2]) << "clk)";
						minelog << "   FPGA_core3: " << result3[0] << "H/s = " << uint32_t(result3[1]) << "hashes / " << result3[3] << "s (" << uint32_t(result3[2]) << "clk)";
					      #else
						minelog << "Mining on PoWhash" << current.headerHash;
					       	minelog	<< "   SW: " << mp;
						minelog << "   FPGA_core0: " << result0[0] << "H/s = " << uint32_t(result0[1]) << "hashes / " << result0[3] << "s (" << uint32_t(result0[2]) << "clk)";
						minelog << "   FPGA_core1: " << result1[0] << "H/s = " << uint32_t(result1[1]) << "hashes / " << result1[3] << "s (" << uint32_t(result1[2]) << "clk)";
						minelog << "   FPGA_core2: " << result2[0] << "H/s = " << uint32_t(result2[1]) << "hashes / " << result2[3] << "s (" << uint32_t(result2[2]) << "clk)";
						minelog << "   FPGA_core3: " << result3[0] << "H/s = " << uint32_t(result3[1]) << "hashes / " << result3[3] << "s (" << uint32_t(result3[2]) << "clk)";
					      #endif

						//calculate previous hashrate 
					        fpga_memwrite("10", 1, 0x32, 0); 	//clear clk cnt and hashcnt
					        fpga_memwrite("10", 1, 0x32, 1); 	//clear clk cnt and hashcnt
					        fpga_memwrite("10", 1, 0x32, 2); 	//clear clk cnt and hashcnt
					        fpga_memwrite("10", 1, 0x32, 3); 	//clear clk cnt and hashcnt
					        fpga_memwrite("00", 1, 0x32, 0); 	//start cnt
					        fpga_memwrite("00", 1, 0x32, 1); 	//start cnt
					        fpga_memwrite("00", 1, 0x32, 2); 	//start cnt
					        fpga_memwrite("00", 1, 0x32, 3); 	//start cnt
					}
					else
						minelog << "Getting work package...";
					if (m_submitHashrate)
					{
						auto rate = mp.rate();
						try
						{
							rpc.eth_submitHashrate(toJS((u256)rate), "0x" + id.hex());
							//cout << (u256)rate << endl;
						}
						catch (jsonrpc::JsonRpcException const& _e)
						{
							cwarn << "Failed to submit hashrate.";
							cwarn << boost::diagnostic_information(_e);
						}
					}

					Json::Value v = rpc.eth_getWork();
					if (v[0].asString().empty())
						throw NoWork();
					
					//Change inhash here
					h256 hh(v[0].asString());
					//h256 hh("f07d862faa6ac480ae03a5e6233e9ba8ffe4e90fb9e84487184f78c717e03dcc");
					//h256 hh("c40699c87f716d5310bf06ce5a4d701bc51df4805e6f540b933d38bfa6cde86e");
					//7bf7715b53a92da9afa0c2751090ecfff51d4a103fd75068a5e17be3b12d095b");
					//3abcbae9418152d05794d9992025be668470ec3be0730f6b189e129bb7845268");
					//c40699c87f716d5310bf06ce5a4d701bc51df4805e6f540b933d38bfa6cde86e");
					//f0c2ba886b3d41d5bb4cd6224ae4694b1a2069e3c9bcd7de0a9f6aba0b0efa0d");
					

					h256 newSeedHash(v[1].asString());
					if (current.seedHash != newSeedHash)
						minelog << "Grabbing DAG for" << newSeedHash;
					if (!(dag = EthashAux::full(newSeedHash, true, [&](unsigned _pc){ cout << "\rCreating DAG. " << _pc << "% done..." << flush; return 0; })))
						BOOST_THROW_EXCEPTION(DAGCreationFailure());
					if (current.seedHash != newSeedHash)
					{

						/* daggen function block*/
						/*
						blockNumber = EthashAux::number(newSeedHash);
						epoch = blockNumber / ETHASH_EPOCH_LENGTH;
						dag_nodes = EthashAux::dataSize(blockNumber) / 64;
						lightcache_nodes = EthashAux::cacheSize_(blockNumber) / 64;
						lightcache_base_index = 0;
						
						
						if (dag_base_index % 2 == 0)
							dag_base_index = lightcache_base_index + lightcache_nodes;
						else
							dag_base_index = lightcache_base_index + lightcache_nodes + 1;
							
						factor_dag = uint32_t(1<<31) / lightcache_nodes;

						std::stringstream epoch_hex;
						epoch_hex << std::hex << epoch;
                                                std::string epoch_h ( epoch_hex.str() );

						std::stringstream dagnodes_hex;
						dagnodes_hex << std::hex << dag_nodes;
                                                std::string dag_nodes_h ( dagnodes_hex.str() );

						std::stringstream lightcache_hex;
						lightcache_hex << std::hex << lightcache_nodes;
                                                std::string lightcache_nodes_h ( lightcache_hex.str() );
							
						std::stringstream lightcache_base_hex;
						lightcache_base_hex << std::hex << lightcache_base_index;
                                                std::string lightcache_base_h ( lightcache_base_hex.str() );

						std::stringstream dag_base_hex;
						dag_base_hex << std::hex << dag_base_index;
                                                std::string dag_base_h ( dag_base_hex.str() );

						std::stringstream factor_dag_hex;
						factor_dag_hex << std::hex << factor_dag;
                                                std::string factor_dag_h ( factor_dag_hex.str() );











						cnote << "epoch" << epoch_h;
						cnote << "dag nodes:" << dag_nodes_h;
						cnote << "lightcache nodes:" << lightcache_nodes_h;
						cnote << "lightcache base:" << lightcache_base_h;
						cnote << "dag base index:" << dag_base_h;
						cnote << "factor dag:" << factor_dag_h;


						cnote << "================= Run DAG Gen ===================";
						fpga_memwrite("00000000", 4, 0x30, 4);  //reset
						fpga_memwrite(epoch_h, 4, 0x00, 4);  //epoch
						fpga_memwrite(dag_nodes_h, 4, 0x20, 4);  //dag_nodes                  "00000096"
						fpga_memwrite(factor_dag_h, 4, 0x28, 4);  //factor                     "000009c0"
						fpga_memwrite(lightcache_nodes_h, 4, 0x2c, 4);  //lightcache_nodes           "000D1FEF"
						fpga_memwrite(lightcache_base_h, 4, 0x34, 4);  //lightcache_base_index      "00000000"
						fpga_memwrite(dag_base_h, 4, 0x38, 4);  //dag_base_index             "000D2053"
						fpga_memwrite("00000000", 4, 0x88, 4);  //dram_base_addr[31:0]       "00000000"
						fpga_memwrite("00000004", 4, 0x8c, 4);  //dram_base_addr[63:32]      "00000004"


						cnote << fpga_memread_return(0x0000000030, false, 4);  //reset
						cnote << "dag_counter";
						cnote << fpga_memread_return(0x0000000040, false, 4);
						cnote << "light_state";
						cnote << fpga_memread_return(0x0000000044, false, 4);
						cnote << "dag_finish:";
						cnote << fpga_memread_return(0x0000000048, false, 4);

						fpga_memwrite("00000001", 4, 0x30, 4);  //reset deassert
						fpga_memwrite("00000003", 4, 0x30, 4);  //start         

        					cnote << " start dag gen!";
        					
						cnote << fpga_memread_return(0x0000000030, false, 4) << "reset";
						cnote << fpga_memread_return(0x0000000000, false, 4) << "epoch";
						cnote << fpga_memread_return(0x0000000020, false, 4) << "dag_nodes";
						cnote << fpga_memread_return(0x0000000028, false, 4) << "factor";
						cnote << fpga_memread_return(0x000000002c, false, 4) << " //lightcache_nodes";
						cnote << fpga_memread_return(0x0000000034, false, 4) << " //lightcache_base_index";
						cnote << fpga_memread_return(0x0000000038, false, 4) << " //dag_base_index";
						cnote << fpga_memread_return(0x0000000088, false, 4) << " //dram_base_addr[31:0]";
						cnote << fpga_memread_return(0x000000008c, false, 4) << "  //dram_base_addr[63:32]";
        					
        					
						//printf("================ Process end ===================\n\n\n");

						*/

						DAGName = (uint8_t *)(newSeedHash.data());
						std::stringstream DAGName_0;
						std::stringstream DAGName_1;
						std::stringstream DAGName_2;
						std::stringstream DAGName_3;
						std::stringstream DAGName_4;
						std::stringstream DAGName_5;
						std::stringstream DAGName_6;
						std::stringstream DAGName_7;
						DAGName_0 << std::setfill('0') << std::setw(2) << std::hex << unsigned(DAGName[0]);
						DAGName_1 << std::setfill('0') << std::setw(2) << std::hex << unsigned(DAGName[1]);
						DAGName_2 << std::setfill('0') << std::setw(2) << std::hex << unsigned(DAGName[2]);
						DAGName_3 << std::setfill('0') << std::setw(2) << std::hex << unsigned(DAGName[3]);
						DAGName_4 << std::setfill('0') << std::setw(2) << std::hex << unsigned(DAGName[4]);
						DAGName_5 << std::setfill('0') << std::setw(2) << std::hex << unsigned(DAGName[5]);
						DAGName_6 << std::setfill('0') << std::setw(2) << std::hex << unsigned(DAGName[6]);
						DAGName_7 << std::setfill('0') << std::setw(2) << std::hex << unsigned(DAGName[7]);
						std::string DAG_f_0 ( DAGName_0.str() );
						std::string DAG_f_1 ( DAGName_1.str() );
						std::string DAG_f_2 ( DAGName_2.str() );
						std::string DAG_f_3 ( DAGName_3.str() );
						std::string DAG_f_4 ( DAGName_4.str() );
						std::string DAG_f_5 ( DAGName_5.str() );
						std::string DAG_f_6 ( DAGName_6.str() );
						std::string DAG_f_7 ( DAGName_7.str() );

						DAG_FileName = "dram-R23-" + DAG_f_0 + DAG_f_1 + DAG_f_2 + DAG_f_3 + DAG_f_4 + DAG_f_5 + DAG_f_6 + DAG_f_7 ;
						char *DAG_FileName_cstr = &DAG_FileName[0];

						//getFileCreationTime("./");
						DAG_FileSize = get_file_size(DAG_FileName);
						cout << DAG_FileName_cstr << endl;
						cout << DAG_FileSize << endl;
						//Program DAG to DRAM
						fpga_xdma_transfer_4cores(DAG_FileSize, DAG_FileName_cstr, 0 ,0x400000000 ,0x400000000);
						//fpga_xdma_transfer(DAG_FileSize, DAG_FileName_cstr, 0 ,0x800000000 ,0x800000000);
						//fpga_xdma_transfer(DAG_FileSize, DAG_FileName_cstr, 0 ,0xc00000000 ,0xc00000000);
						//fpga_xdma_transfer(DAG_FileSize, DAG_FileName_cstr, 0 ,0x1000000000 ,0x1000000000);
						
						val_num_pages = (uint32_t)(DAG_FileSize / 128 );//26804203; //26738687; //26673149; //26738688;//3422551944; //26476543;

						//np << std::hex << val_num_pages;
						//std::string num_page ( np.str() );
						num_page = int_to_hex(val_num_pages);

						uint32_t val_factor = uint32_t(1<<31) / val_num_pages;
						//fac << std::hex << val_factor;
						//std::string factor ( fac.str() );
						factor = int_to_hex(val_factor);

						cnote << "num_page:" << num_page;
						cnote << "factor:" << factor;
						cnote << "program new dram to FPGA";
						std::string finish = "0";
						/* daggen function block */
						/*
						while (finish != "00000001")
						{
							finish = fpga_memread_return(0x0000000048, false, 4);
							//cnote << finish;
						}
						cnote << "Finish!";
						*/
						Reset_Core(255);
						//xdma_reg_read(map_base, 0x00000048, &reg_read_data, core_select)
						//fpga_dram_read(DAG_FileSize, DAG_FileName_cstr, 0 ,0x400000000 ,0x400000000);
					}
					if (m_precompute)
						EthashAux::computeFull(sha3(newSeedHash), true);
					if (hh != current.headerHash)
					{

						current.headerHash = hh;
						current.seedHash = newSeedHash;
						
						//current.boundary = h256(fromHex("0000F2006df37f675ef6eadf5ab9a2072d44268d97df837e6748956e5c6c2116"), h256::AlignRight);
						current.boundary = h256(fromHex(v[2].asString()), h256::AlignRight);
						
						minelog << "Got work package:";
						minelog << "  Header-hash:" << current.headerHash.hex();
						minelog << "  Seedhash:" << current.seedHash.hex();
						minelog << "  Target: " << h256(current.boundary).hex();
						value32 = (uint32_t*)(current.headerHash.data());

						std::string keccak_input_hash = current.headerHash.hex();
						
						
					     #ifdef SINGLE_HASH
						std::string compare  = "ffffffffffffffff";
					     #else
						std::string compare = h256(current.boundary).hex();
						//std::string compare = "0123456789abcdef";
					     #endif
						


						std::stringstream kec7;
						std::stringstream kec6;
						std::stringstream kec5;
						std::stringstream kec4;
						std::stringstream kec3;
						std::stringstream kec2;
						std::stringstream kec1;
						std::stringstream kec0;
                                                kec0 << std::hex << value32[0];
                                                std::string kecc0 ( kec0.str() );
                                                kec1 << std::hex << value32[1];
                                                std::string kecc1 ( kec1.str() );
                                                kec2 << std::hex << value32[2];
                                                std::string kecc2 ( kec2.str() );
                                                kec3 << std::hex << value32[3];
                                                std::string kecc3 ( kec3.str() );
                                                kec4 << std::hex << value32[4];
                                                std::string kecc4 ( kec4.str() );
                                                kec5 << std::hex << value32[5];
                                                std::string kecc5 ( kec5.str() );
                                                kec6 << std::hex << value32[6];
                                                std::string kecc6 ( kec6.str() );
                                                kec7 << std::hex << value32[7];
                                                std::string kecc7 ( kec7.str() );

						Reset_Core(255);
						for (int j=0; j<=3; j++)
						{
							//fpga_memwrite("00000000", 4, 0x30, j); 	//reset

							fpga_memwrite(kecc0, 4, 0x00, j);  //inhash[255:0]
                                                	fpga_memwrite(kecc1, 4, 0x04, j);  //inhash[255:0]
                                                	fpga_memwrite(kecc2, 4, 0x08, j);  //inhash[255:0]
                                                	fpga_memwrite(kecc3, 4, 0x0c, j);  //inhash[255:0]
                                                	fpga_memwrite(kecc4, 4, 0x10, j);  //inhash[255:0]
                                                	fpga_memwrite(kecc5, 4, 0x14, j);  //inhash[255:0]
                                                	fpga_memwrite(kecc6, 4, 0x18, j);  //inhash[255:0]
                                                	fpga_memwrite(kecc7, 4, 0x1c, j);  //inhash[255:0]

							fpga_memwrite(startnonce[j], 8, 0x20, j); 	 //innounce[63:0]
	    	    					fpga_memwrite(factor + "0F", 4, 0x28, j);   //factor[6:0] + num_zeroes[3:0]
	    	    					fpga_memwrite(num_page, 4, 0x2c, j); 	 //num_pages[31:0]
	    	    					fpga_memwrite(stopnonce[j], 8, 0x34, j); 	 //stopnonce[63:0]
	    	    					fpga_memwrite(compare, 8, 0x80, j); 	 //stopnonce[63:0]
							fpga_memwrite("00000001", 4, 0x30, j);      //deassert reset


					     	#ifdef FPGA_Mining_Only
							fpga_memwrite("00000003", 4, 0x30, j);      //start
					    	#elif SW_Mining_Only	
							f.setWork(current);
					    	#else
							fpga_memwrite("00000003", 4, 0x30, j);      //start
							f.setWork(current);
					     	#endif
						}

					}
					this_thread::sleep_for(chrono::milliseconds(_recheckPeriod));
				}
				if (completed == true)
				{	cnote << "Solution found; Submitting to" << _remote << "...";
					cnote << "  Nonce:" << solution.nonce.hex();
					cnote << "  Mixhash:" << solution.mixHash.hex();
					cnote << "  Header-hash:" << current.headerHash.hex();
					cnote << "  Seedhash:" << current.seedHash.hex();
					cnote << "  Target: " << h256(current.boundary).hex();
					cnote << "  Ethash: " << h256(EthashAux::eval(current.seedHash, current.headerHash, solution.nonce).value).hex();
					cout << toString(solution.nonce) << endl;
					cout << toString(current.headerHash) << endl;
					cout << toString(solution.mixHash) << endl;

					if (EthashAux::eval(current.seedHash, current.headerHash, solution.nonce).value < current.boundary)
					{
						bool ok = rpc.eth_submitWork("0x" + toString(solution.nonce), "0x" + toString(current.headerHash), "0x" + toString(solution.mixHash));
						if (ok)
							cnote << "B-) Submitted and accepted.";
						else
							cwarn << ":-( Not accepted.";
					}
					else
						cwarn << "FAILURE: GPU gave incorrect result!";
					current.reset();
				}
				else if (intr_status(0) == "00000001")
				{	
			       	 	Read_Outhash(0);

					Reset_Core(0);
					Set_StartNonce( out_nonce[1]+out_nonce[0], 0);
					Start_Core(0);

					cnote << "FPGA Solution found; Submitting to" << _remote << "...";
					cnote << "  Nonce:" << out_nonce[1] << out_nonce[0];
					cnote << "  Mixhash:" << solution.mixHash.hex();
					cnote << "  FPGA Mixhash:" << out_mixhash[0] << out_mixhash[1] << out_mixhash[2] << out_mixhash[3] << out_mixhash[4] << out_mixhash[5] << out_mixhash[6] << out_mixhash[7] ;
					cnote << "  Header-hash:" << current.headerHash.hex();
					cnote << "  FPGA Header-hash:" << out_headerhash[0] << out_headerhash[1] << out_headerhash[2] << out_headerhash[3] << out_headerhash[4] << out_headerhash[5] << out_headerhash[6] << out_headerhash[7];
					cnote << "  Compare: " << compare[1] << compare[0];
					cnote << "  Outhash2: " << out_outhash2[0] << out_outhash2[1] << out_outhash2[2] << out_outhash2[3] << out_outhash2[4] << out_outhash2[5] << out_outhash2[6] << out_outhash2[7];
					cnote << "  Ethash: " << h256(EthashAux::eval(current.seedHash, current.headerHash, solution.nonce).value).hex();
					fpga_memwrite("00000000", 4, 0x30, 0);     //reset
					bool ok = rpc.eth_submitWork("0x" + out_nonce[1]+out_nonce[0], 
							"0x" + out_headerhash[0]+out_headerhash[1]+out_headerhash[2]+out_headerhash[3]+out_headerhash[4]+out_headerhash[5]+out_headerhash[6]+out_headerhash[7], 
							"0x" + out_mixhash[0]+out_mixhash[1]+out_mixhash[2]+out_mixhash[3]+out_mixhash[4]+out_mixhash[5]+out_mixhash[6]+out_mixhash[7]);
					if (ok)
					{	
						cnote << "B-) Submitted and accepted.";
						std::cout << "core0: B-) Submitted and accepted." << endl;
					}
					else
					{
						cwarn << ":-( Not accepted.";
						std::cout << "core0: :-( Not accepted." << endl;
					}
					// Time computation here
   					 //auto end = std::chrono::system_clock::now();
   	 				 //std::chrono::duration<double> elapsed_seconds = end-start;
    					 //std::time_t end_time = std::chrono::system_clock::to_time_t(end);
					 //std::cout << "finished computation at " << std::ctime(&end_time)
              				 //<< "elapsed time: " << elapsed_seconds.count() << "s\n";
					//std::cout << intr_status(0);
					//std::cout << intr_status(0);

				}

				else if (intr_status(1) == "00000001")
				{	
					Read_Outhash(1);

					Reset_Core(1);
					Set_StartNonce( out_nonce[1]+out_nonce[0], 1);
					Start_Core(1);

			       	 	cnote << "FPGA Solution found; Submitting to" << _remote << "...";
					cnote << "  Nonce:" << out_nonce[1] << out_nonce[0];
					cnote << "  Mixhash:" << solution.mixHash.hex();
					cnote << "  FPGA Mixhash:" << out_mixhash[0] << out_mixhash[1] << out_mixhash[2] << out_mixhash[3] << out_mixhash[4] << out_mixhash[5] << out_mixhash[6] << out_mixhash[7];
					cnote << "  Header-hash:" << current.headerHash.hex();
					cnote << "  FPGA Header-hash:" << out_headerhash[0] << out_headerhash[1] << out_headerhash[2] << out_headerhash[3] << out_headerhash[4] << out_headerhash[5] << out_headerhash[6] << out_headerhash[7];
					cnote << "  Compare: " << compare[1] << compare[0];
					cnote << "  Outhash2: " << out_outhash2[0] << out_outhash2[1] << out_outhash2[2] << out_outhash2[3] << out_outhash2[4] << out_outhash2[5] << out_outhash2[6] << out_outhash2[7];
					cnote << "  Ethash: " << h256(EthashAux::eval(current.seedHash, current.headerHash, solution.nonce).value).hex();
					fpga_memwrite("00000000", 4, 0x30, 1);     //reset
					bool ok = rpc.eth_submitWork("0x" + out_nonce[1]+out_nonce[0], 
							"0x" + out_headerhash[0]+out_headerhash[1]+out_headerhash[2]+out_headerhash[3]+out_headerhash[4]+out_headerhash[5]+out_headerhash[6]+out_headerhash[7], 
							"0x" + out_mixhash[0]+out_mixhash[1]+out_mixhash[2]+out_mixhash[3]+out_mixhash[4]+out_mixhash[5]+out_mixhash[6]+out_mixhash[7]);
					if (ok)
					{	
						cnote << "B-) Submitted and accepted.";
						std::cout << "core1: B-) Submitted and accepted." << endl;
					}
					else
					{
						cwarn << ":-( Not accepted.";
						std::cout << "core1: :-( Not accepted." << endl;
					}
					// Time computation here
   					 //auto end = std::chrono::system_clock::now();
   	 				 //std::chrono::duration<double> elapsed_seconds = end-start;
    					 //std::time_t end_time = std::chrono::system_clock::to_time_t(end);
					 //std::cout << "finished computation at " << std::ctime(&end_time)
              				 //<< "elapsed time: " << elapsed_seconds.count() << "s\n";
					//std::cout << intr_status(1);
					//std::cout << intr_status(1);

				}

				else if (intr_status(2) == "00000001")
				{	
					Read_Outhash(2);

					Reset_Core(2);
					Set_StartNonce( out_nonce[1]+out_nonce[0], 2);
					Start_Core(2);

					cnote <<  "FPGA Solution found; Submitting to" << _remote << "...";
					cnote <<  "  Nonce:" << out_nonce[1] << out_nonce[0];
					cnote <<  "  Mixhash:" << solution.mixHash.hex();
					cnote <<  "  FPGA Mixhash:" << out_mixhash[0] << out_mixhash[1] << out_mixhash[2] << out_mixhash[3] << out_mixhash[4] << out_mixhash[5] << out_mixhash[6] << out_mixhash[7];
					cnote <<  "  Header-hash:" << current.headerHash.hex();
					cnote <<  "  FPGA Header-hash:" << out_headerhash[0] << out_headerhash[1] << out_headerhash[2] << out_headerhash[3] << out_headerhash[4] << out_headerhash[5] << out_headerhash[6] << out_headerhash[7];
					cnote <<  "  Compare: " << compare[1] << compare[0];
					cnote <<  "  Outhash2: " << out_outhash2[0] << out_outhash2[1] << out_outhash2[2] << out_outhash2[3] << out_outhash2[4] << out_outhash2[5] << out_outhash2[6] << out_outhash2[7];
					cnote <<  "  Ethash: " << h256(EthashAux::eval(current.seedHash, current.headerHash, solution.nonce).value).hex();
					fpga_memwrite("00000000", 4, 0x30, 2);     //reset
					bool ok = rpc.eth_submitWork("0x" + out_nonce[1]+out_nonce[0], 
							"0x" + out_headerhash[0]+out_headerhash[1]+out_headerhash[2]+out_headerhash[3]+out_headerhash[4]+out_headerhash[5]+out_headerhash[6]+out_headerhash[7], 
							"0x" + out_mixhash[0]+out_mixhash[1]+out_mixhash[2]+out_mixhash[3]+out_mixhash[4]+out_mixhash[5]+out_mixhash[6]+out_mixhash[7]);
					if (ok)
					{
						cnote << "B-) Submitted and accepted.";
						std::cout << "core2: B-) Submitted and accepted." << endl;
					}
					else
					{
						cwarn << ":-( Not accepted.";
						std::cout << "core2: :-( Not accepted." << endl;
					}
					// Time computation here
   					 //auto end = std::chrono::system_clock::now();
   	 				 //std::chrono::duration<double> elapsed_seconds = end-start;
    					 //std::time_t end_time = std::chrono::system_clock::to_time_t(end);
					 //std::cout << "finished computation at " << std::ctime(&end_time)
              				 //<< "elapsed time: " << elapsed_seconds.count() << "s\n";
					//std::cout << intr_status(2);
					//std::cout << intr_status(2);

				}

				else if (intr_status(3) == "00000001")
				{	
			       	 	Read_Outhash(3);

					Reset_Core(3);
					Set_StartNonce( out_nonce[1]+out_nonce[0], 3);
					Start_Core(3);

					cnote << "FPGA Solution found; Submitting to" << _remote << "...";
					cnote << "  Nonce:" << out_nonce[1] << out_nonce[0];
					cnote << "  Mixhash:" << solution.mixHash.hex();
					cnote << "  FPGA Mixhash:" << out_mixhash[0] << out_mixhash[1] << out_mixhash[2] << out_mixhash[3] << out_mixhash[4] << out_mixhash[5] << out_mixhash[6] << out_mixhash[7];
					cnote << "  Header-hash:" << current.headerHash.hex();
					cnote << "  FPGA Header-hash:" << out_headerhash[0] << out_headerhash[1] << out_headerhash[2] << out_headerhash[3] << out_headerhash[4] << out_headerhash[5] << out_headerhash[6] << out_headerhash[7];
					cnote << "  Compare: " << compare[1] << compare[0];
					cnote << "  Outhash2: " << out_outhash2[0] << out_outhash2[1] << out_outhash2[2] << out_outhash2[3] << out_outhash2[4] << out_outhash2[5] << out_outhash2[6] << out_outhash2[7];
					cnote << "  Ethash: " << h256(EthashAux::eval(current.seedHash, current.headerHash, solution.nonce).value).hex();
					fpga_memwrite("00000000", 4, 0x30, 3);     //reset
					bool ok = rpc.eth_submitWork("0x" + out_nonce[1]+out_nonce[0], 
							"0x" + out_headerhash[0]+out_headerhash[1]+out_headerhash[2]+out_headerhash[3]+out_headerhash[4]+out_headerhash[5]+out_headerhash[6]+out_headerhash[7], 
							"0x" + out_mixhash[0]+out_mixhash[1]+out_mixhash[2]+out_mixhash[3]+out_mixhash[4]+out_mixhash[5]+out_mixhash[6]+out_mixhash[7]);
					if (ok)
					{
						cnote << "B-) Submitted and accepted.";
						std::cout << "core3: B-) Submitted and accepted." << endl;
					}
					else
					{
						cwarn << ":-( Not accepted.";
						std::cout << "core3: :-( Not accepted." << endl;
					}
					// Time computation here
   					 //auto end = std::chrono::system_clock::now();
   	 				 //std::chrono::duration<double> elapsed_seconds = end-start;
    					 //std::time_t end_time = std::chrono::system_clock::to_time_t(end);
					 //std::cout << "finished computation at " << std::ctime(&end_time)
              				 //<< "elapsed time: " << elapsed_seconds.count() << "s\n";
					//std::cout << intr_status(3);
					//std::cout << intr_status(3);
				}
					
				
			}
			catch (jsonrpc::JsonRpcException&)
			{
				for (auto i = 3; --i; this_thread::sleep_for(chrono::seconds(1)))
					cerr << "JSON-RPC problem. Probably couldn't connect. Retrying in " << i << "... \r";
				cerr << endl;
			}
			catch (NoWork&)
			{
				this_thread::sleep_for(chrono::milliseconds(100));
			}
		exit(0);
	}

	/// Operating mode.
	OperationMode mode;

	/// Mining options
	std::string m_minerType = "cpu";
	unsigned m_miningThreads = UINT_MAX;
	uint64_t m_currentBlock = 0;

	/// DAG initialisation param.
	unsigned m_initDAG = 0;

	/// Benchmarking params
	unsigned m_benchmarkWarmup = 3;
	unsigned m_benchmarkTrial = 3;
	unsigned m_benchmarkTrials = 5;

	/// Farm params
	string m_farmURL = "http://127.0.0.1:8545";
	unsigned m_farmRecheckPeriod = 500;
	bool m_precompute = true;
	bool m_submitHashrate = true;
};
